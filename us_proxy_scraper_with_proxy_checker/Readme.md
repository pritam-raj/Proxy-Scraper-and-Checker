# Python Script For scrap Free Proxy From [Url](https://us-proxy.org)

Splash & scrapy is a Python libraries that used for **Scraping** .

## Installation
Please use docker to active splash and keep localhost same (http://192.168.99.100:8050)
* install docker for windows [Click Here](https://github.com/docker/toolbox/releases)
* Run docker and install splash ![Splash](ScreenShot_20200901110920.png "Splash Install")
* Start Splash and Keep port same ![Splash](ScreenShot_20200901110953.png "Splash Run")

```bash
Unzip and open us_proxy_scraper Folder where scrapy.cfg file located
```

## Usage

```python
pip install -r Requirements.txt

Scrapy crawl usproxy -o filename.csv
```
When we have proxyfile.csv Run 
```python
python proxy_checker.py 
```
* And Follow instructions
* Its automatically Create good.txt file in same folder which contains 100% working proxies 

###### Contributing
  Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

**[@Pritam Raj](https://www.linkedin.com/in/pritam-raj-pandey-62203a165/)**


